import { useNavigate } from "react-router-dom";
import "./Header.css";
export function Header() {
  const navigate = useNavigate();

  return (
    <div className="container">
      <ul className="list-items">
        <li onClick={() => navigate("/")} className="list-item">
          FlashCard
        </li>
        <li onClick={() => navigate("/")} className="list-item">
          Decks
        </li>
      </ul>
    </div>
  );
}
