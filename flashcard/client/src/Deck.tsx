import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { createCard } from "./api/createCard";
import { getDeck } from "./api/getDeck";
import { deleteCard } from "./api/deleteCard";
import "./App.css";
import { Header } from "./Header";
import { DeckType } from "./App";

export default function Deck() {
  const [cards, setCards] = useState<string[]>([]);
  const [deck, setDeck] = useState<DeckType>();
  const [text, setText] = useState("");
  let { deckId } = useParams();

  async function handleCreateDeck(e: React.FormEvent) {
    e.preventDefault();
    const { cards: serverCards } = await createCard(deckId!, text);
    setCards(serverCards);
    setText("");
  }

  async function handleDeleteCard(index: number) {
    if (!deckId) return;
    await deleteCard(deckId, index);
    setCards(cards.filter((card, i) => i != index));
  }

  useEffect(() => {
    async function fetchDeck() {
      if (!deckId) return;
      const newDeck = await getDeck(deckId);
      setDeck(newDeck);
      setCards(newDeck.cards);
    }
    fetchDeck();
  }, [deckId]);

  return (
    <div className="App">
      <h1>{deck?.title}</h1>
      <Header />
      <ul className="decks">
        {cards.map((card: string, index: number) => (
          <li key={index}>
            <button onClick={() => handleDeleteCard(index)}>X</button>
            {card}
          </li>
        ))}
      </ul>
      <form onSubmit={handleCreateDeck}>
        <label htmlFor="deck-title">Card Title</label>
        <input
          id="deck-title"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setText(e.target.value);
          }}
          value={text}
        />
        <button>Create Card</button>
      </form>
    </div>
  );
}
