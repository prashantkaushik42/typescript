import { DeckType } from "../App";
import { API_URL } from "./config";

export async function createCard(
  deckId: string,
  text: string
): Promise<DeckType> {
  const response = await fetch(`${API_URL}/decks/${deckId}/cards`, {
    method: "post",
    headers: {
      "content-type": "application/json",
    },
    body: JSON.stringify({
      text,
    }),
  });
  return response.json();
}
