import { Route, Routes } from "react-router-dom";
import EditorPage from "./pages/EditorPage";
import Home from "./pages/Home";
import {Toaster} from 'react-hot-toast'

function App() {
  return (
    <>
    <Toaster 
       position="top-right"
       toastOptions={{
               success: {
                       duration: 3000,
                       theme: '#4aed88'
                   }
           }}
    />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/editor/:roomId" element={<EditorPage />} />
      </Routes>
    </>
  );
}

export default App;
