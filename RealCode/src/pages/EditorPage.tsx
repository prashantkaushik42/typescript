import { useState } from "react";
import Client from "../components/Client";
import Editor from "../components/Editor";

const EditorPage = () => {
  const [clients, setClients] = useState([
    { socketId: 1, username: 'Prashant K' },
    { socketId: 2, username: 'John Cena' }
  ])
  return (
    <div className="grid">
      <div>
        <div>
          <div>
            <img src="/code-sync.png" alt='code' />
          </div>
          <h3>Connected</h3>
          <div>
            {
              clients.map((client) => (
              <Client key={client.socketId} username={client.username} />
              ))
            }
          </div>
        </div>
        <button className="m-2 bg-red-100">COPY ROOM ID</button>
        <button className="bg-red-100">Leave</button>
      </div>
      <div>
        <Editor /> 
      </div>
    </div>
  )
};

export default EditorPage;
