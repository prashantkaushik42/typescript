import { KeyboardEvent, KeyboardEventHandler, MouseEvent, useState } from "react";
import { v4 as uuidV4 } from "uuid";
import toast from 'react-hot-toast'
import { useNavigate } from "react-router-dom";

const Home = () => {
    const [roomId, setRoomId] = useState("");
    const [username, setUsername] = useState("");
    const navigate = useNavigate()

    function createNewRoom(e: MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        const id = uuidV4();
        setRoomId(id);
        toast.success("Created a new Room")
    }

    function joinRoom() {
        if (!roomId || !username) {
            toast.error("ROOM ID and Username is required")
            return
        }
        // redirect 
        navigate(`/editor/${roomId}`, {
            state: {
                username,
            }
        }
        )
    }

    function joinRoomWithEnter(e: KeyboardEvent) {
        if (e.code === 'Enter') {
            joinRoom()
        }
    }

    return (
        <div className="flex justify-center items-center h-[100vh]">
            <div className="bg-[#0e172c] rounded-md w-4/12 py-4 shadow-md shadow-gray-200">
                <div className="ml-6">
                    <img className="h-20" src="/code-sync.png" alt="Logo" />
                </div>
                <div className="">
                    <p className="mb-1 text-center text-gray-100">
                        Paste Invitation Room ID
                    </p>
                    <div className="flex flex-col gap-2 mx-6 ">
                        <input
                            className="rounded-sm px-4 border border-solid border-background"
                            type="text"
                            placeholder="ROOM ID"
                            onChange={(e) => setRoomId(e.target.value)}
                            value={roomId}
                        />
                        <input
                            className="rounded-sm px-4 border border-solid border-background"
                            type="text"
                            placeholder="USERNAME"
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            onKeyUp={joinRoomWithEnter}
                        />
                    </div>
                </div>
                <div className="my-2 flex justify-end mr-6">
                    <button onClick={joinRoom} onKeyUp={joinRoomWithEnter} className="bg-[#2a9d8f] rounded-md p-2 px-4">Join</button>
                </div>
                <div>
                    <p className="text-gray-100 text-center">
                        If you don't have an invite then{" "}
                        <span
                            className="text-green-800 cursor-pointer"
                            onClick={createNewRoom}
                        >
                            create room
                        </span>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Home;
