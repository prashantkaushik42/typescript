import Avatar from "react-avatar"

type clientProps = {
    username: string,
  }

const Client = ({username}: clientProps) => {
  return (
    <div>
      <Avatar name={username} size="50" round="14px" />
      <span>{username}</span>
    </div>
  )
}

export default Client
