/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        background: "#16161a",
        highlight: "#7f5af0",
        secondary: "#f25f4c",
        tertiary: "#e53170",
        cyan: "#8bd3dd",
        stroke: "#001858",
        headline: "#001858",
      },
    },
  },
  plugins: [],
};
